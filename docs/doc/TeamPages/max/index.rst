===============
Max's team page
===============
:author: Max van Eijndhoven
:date: 03 Dec 2020

(11/12/2020) Starting up
######################################

Install the latest version of Python:
`<https://www.python.org/>`_

Install **pip** via python command promp (recommended version is python 3)::

   python get-pip.py

Install **flask** via pip for running the website (`<https://flask.palletsprojects.com/en/1.1.x/installation/>`_)::

   pip install Flask

Install **Sphinx** for the building the documentation (`<https://www.sphinx-doc.org/en/master/usage/installation.html>`_)::

   pip install -U sphinx

*******************
Running the project
*******************

Run sphinx with the following command::

   sphinx-build -b html docs/doc builddir

Run jinja with the following command::

   cd site
   flask run --host=0.0.0.0 --port=5000


.. toctree::
   :glob:
   :maxdepth: 2

 
